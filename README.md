# Description
  
This is a simple program which simulates (if you can pretend) what
the dynamic linker does.  It will take command line arguments for
a shared object, a command (from that shared object), and a series
of arguments which will be executed during runtime.
  
The idea here is that the shared object will be loaded and linked
with a call to dlopen and the command will be located via a call
to dlsym.  


